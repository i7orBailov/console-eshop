﻿
namespace ConsoleEShop.PL
{
    class UserEntity
    {
        public string Password { get; }
        public string Mail { get; }
        
        public UserEntity(string password, string mail)
        {
            Password = password;
            Mail = mail;
        }
    }
}
