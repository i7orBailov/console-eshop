﻿using System;

namespace ConsoleEShop.PL.Menues
{
    class UserMenue : AuthorizedUserMenue
    {
        public UserMenue(UserEntity user) : base(user) { }

        readonly static string[] options = new string[]
        {
            "View products list",
            "Find product by name",
            "Add order to wish list",
            "Process order",
            "Set order status to 'Received'",
            "Cancel order",
        };

        public override void Invoke()
        {
            Greeting("Here we go!", "User");
            while (true)
            {
                OptionSelector("Please, choose an option:", options);
                Console.Write("Number of option: ");
                bool correctCommand = int.TryParse(Console.ReadLine(), out int commandChoice);
                if (correctCommand)
                {
                    Console.Clear();
                    switch (commandChoice)
                    {
                        case 1:
                            {
                                ViewProductsList();
                                continue;
                            }
                        case 2:
                            {
                                FindProductByName();
                                continue;
                            }
                        case 3:
                            {
                                CreateNewOrder();
                                continue;
                            }
                        case 4:
                            {
                                ProcessOrder();
                                continue;
                            }
                        case 5:
                            {
                                SetOrderStatus();
                                continue;
                            }
                        case 6:
                            {
                                CancelOrder();
                                continue;
                            }
                        case 7:
                                break;
                        default:
                            {
                                NotifyError("Unknown number of option.");
                                continue;
                            }
                    }
                    break;
                }
                else
                {
                    NotifyError("Incorrect value.");
                    continue;
                }
            }
        }
    }
}
