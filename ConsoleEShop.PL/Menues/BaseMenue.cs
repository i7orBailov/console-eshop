﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleEShop.BL.Controllers;

namespace ConsoleEShop.PL.Menues
{
    class BaseMenue
    {
        readonly BaseFunctionsController controller = new BaseFunctionsController();

        public virtual void Invoke() { }
        
        protected void OptionSelector(string introduction, IEnumerable<string> optionsList)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(introduction);
            Console.ResetColor();

            int counter = 1;
            foreach (var option in optionsList)
                Console.WriteLine("\t{0}: {1}", counter++, option);
            
        }

        protected void Greeting(string introduction, string userStatus)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(introduction + " You signed as ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(userStatus);
            Console.ResetColor();
        }

        protected void ViewProductsList()
        {
            var productsList = controller.ViewProductsList();
            if (productsList.Count() == 0)
                NotifyError("No available products.");
            else
            {
                int productCounter = 1;
                foreach (var product in productsList)
                {
                    Console.WriteLine($"{productCounter}. " +
                                      $"Product: {product.Name}\n" +
                                      $"Category: {product.Category}\n" +
                                      $"Price: {product.Price}\n" +
                                      $"Description: {product.Description}\n");
                    productCounter++;
                }
            }
        }

        protected void FindProductByName()
        {
            Console.Write("Product name: ");
            string productToFind = Console.ReadLine();
            var product = controller.FindProductByName(productToFind);
            if (product is null)
                NotifyError("No such a product was found.");
            else
            {
                Console.WriteLine($"Product: {product.Name}\n" +
                                  $"Category: {product.Category}\n" +
                                  $"Price: {product.Price}\n" +
                                  $"Description: {product.Description}\n");
            }
        }

        protected void NotifyError(string errorMessage)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(errorMessage);
            Console.ResetColor();
        }
    }
}
