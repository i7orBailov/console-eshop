﻿using ConsoleEShop.BL.Controllers;
using System;

namespace ConsoleEShop.PL.Menues
{
    class VisitorMenue : BaseMenue
    {
        readonly VisitorController controller = new VisitorController();

        readonly static string[] options = new string[]
        {
            "View products list",
            "Find product by name",
            "Register new account",
            "Log into existing account",
            "Exit"
        };

        public override void Invoke()
        {
            Greeting("Welcome to the shop!", "Visitor");
            while (true)
            {
                OptionSelector("Please, choose an option:", options);
                Console.Write("Number of option: ");
                bool correctCommand = int.TryParse(Console.ReadLine(), out int commandChoice);
                if (correctCommand)
                {
                    Console.Clear();
                    switch (commandChoice)
                    {
                        case 1:
                            {
                                ViewProductsList();
                                continue;
                            }
                        case 2:
                            {
                                FindProductByName();
                                continue;
                            }
                        case 3:
                            {
                                Register();
                                continue;
                            }
                        case 4:
                            {
                                LogIn();
                                continue;
                            }
                        case 5:
                                break;
                        default:
                            {
                                NotifyError("Unknown number of option.");
                                continue;
                            }
                    }
                    break;
                }
                else
                {
                    NotifyError("Incorrect value.");
                    continue;
                }
            }
        }

        void Register()
        {
            Console.Write("Password: ");
            string password = Console.ReadLine();
            Console.Write("Email: ");
            string email = Console.ReadLine();
            bool successfullyRegistered = controller.Register(password, email);
            if (successfullyRegistered)
            {
                UserEntity user = new UserEntity(password, email);
                UserMenue userMenue = new UserMenue(user);
                Console.Clear();
                userMenue.Invoke();
            }
            else
                NotifyError("Could not register user with such a credentials. Already registered?");
        }

        void LogIn()
        {
            Console.Write("Password: ");
            string password = Console.ReadLine();
            Console.Write("Email: ");
            string email = Console.ReadLine();
            bool successfullyLogged = controller.LogIn(password, email);
            if (successfullyLogged)
            {
                UserEntity user = new UserEntity(password, email);
                UserMenue userMenue = new UserMenue(user);
                Console.Clear();
                userMenue.Invoke();
            }
            else
                NotifyError("Could not log into account. Not registered yet?");
        }
    }
}
