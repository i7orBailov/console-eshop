﻿using ConsoleEShop.BL.Controllers;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop.PL.Menues
{
    class AuthorizedUserMenue : BaseMenue
    {
        protected readonly UserEntity currentUser;
        readonly AuthorizedUserController controller = new AuthorizedUserController();
        public AuthorizedUserMenue(UserEntity user)
        {
            currentUser = user;
        }


        protected void CreateNewOrder()
        {
            Console.Write("Order name: ");
            string orderName = Console.ReadLine();
            bool orderAddedToWishList = controller.CreateNewOrder(orderName, currentUser.Mail);
            if (orderAddedToWishList)
                NotifyInfo("Added to wish list.");
            else
                NotifyError("No such products currently available.");
        }

        protected void ProcessOrder()
        {
            Console.Write("Order id: ");
            bool correctId = int.TryParse(Console.ReadLine(), out int orderId);
            if (correctId)
            {
                bool orderSentForProcessing = controller.ProcessOrder(orderId, currentUser.Mail);
                if (orderSentForProcessing)
                    NotifyInfo("Order sent for processing.");
                else
                    NotifyError("No such orders found in wish list.");
            }
            else
                NotifyError("Incorrect value.");
        }

        protected virtual void SetOrderStatus()
        {
            Console.Write("Order id: ");
            bool correctId = int.TryParse(Console.ReadLine(), out int orderId);
            if (correctId)
            {
                bool orderStatusSuccessfullySet = controller.SetOrderStatus(orderId, currentUser.Mail);
                if (orderStatusSuccessfullySet)
                    NotifyInfo("Order status sucessfully altered to 'Received' Thanks for the purchase!");
                else
                    NotifyError("No such orders found in processing list.");
            }
            else
                NotifyError("Incorrect value.");
        }
        
        protected void CancelOrder()
        {
            Console.WriteLine("Order id: ");
            bool correctId = int.TryParse(Console.ReadLine(), out int orderId);
            if (correctId)
            {
                bool orderSuccessfullyCancelled = controller.CancelOrder(orderId, currentUser.Mail);
                if (orderSuccessfullyCancelled)
                    NotifyInfo("Order successfully cancelled.");
                else
                    NotifyError("No such orders found in wish list.");
            }
            else
                NotifyError("Incorrect value.");
        }

        protected bool CheckIfUserIsAdmin(UserEntity user) =>
            controller.CheckIfUserIsAdmin(user.Password, user.Mail);

        protected void NotifyInfo(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}
