﻿using System;
using ConsoleEShop.PL.Menues;

namespace ConsoleEShop.PL
{
    class Program
    {
        static void Main(string[] args)
        {
            VisitorMenue mainMenue = new VisitorMenue();
            mainMenue.Invoke();
        }
    }
}
