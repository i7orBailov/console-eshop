﻿using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Enums;
using System.Collections.Generic;

namespace ConsoleEShop.DAL.DateBase
{
    public static class ProductsDB
    {
        public static IEnumerable<Product> Products = new Product[]
        {
            new Product("CD-60 Dread V3 DS",
                        ProductCategories.AcousticGuitars,
                        "The CD-60 is a genuine Fender that is affordable and comes in three great-looking finishes to match your style.",
                        7000),
            new Product("Player Stratocaster",
                        ProductCategories.ElectricGuitars,
                        "Featuring this classic sound—bell-like high end, punchy mids and robust low end, combined with crystal-clear" +
                        "articulation—the Player Stratocaster is packed with authentic Fender feel and style",
                        9000),
            new Product("Vintera` 60s Jazz Bass",
                        ProductCategories.ElectricBasses,
                        "A vintage-style, 4-saddle bridge with threaded saddles and reverse, open-gear tuning machines provide" +
                        "original-era aesthetics, rock-solid performance and tuning stability. ",
                        5000),
            new Product("Newporter Special Mahogany",
                        ProductCategories.AcousticGuitars,
                        "The Newporter Special’s no-compromise attitude extends to its build quality, featuring quartersawn scalloped X bracing" +
                        "for superior resonance, an upgraded bone nut and saddle, and a tilt-back headstock for enhanced sustain.",
                        6000),
            new Product("Tone Master DeLuxe Reverb",
                        ProductCategories.GuitarAmplifiers,
                        "In a bold Fender first, the Tone Master Deluxe Reverb amplifier uses massive digital processing power to achieve a" +
                        "single remarkable sonic feat: faithfully modeling the circuitry and 22-watt power output of an original Deluxe tube amp.",
                        2700),
            new Product("Jasmine S-34C Cutaway Acoustic Guitar Natural",
                        ProductCategories.AcousticGuitars,
                        "The Jasmine S-34C is a stylish grand orchestra-style guitar with a rich, well-balanced sound and a graceful Venetian-style" +
                        "cutaway that represent exceptional value.",
                        1700),
            new Product("Kramer Nightswan Electric Guitar Black/Blue Polka Dot",
                        ProductCategories.ElectricGuitars,
                        "The bolt-on 3-piece hard maple neck has a “C” profile with ebony fingerboard, a 24.75” scale, medium jumbo frets, a 16” radius," +
                        "Floyd Rose RS lock nut, and a unique wave pattern of pearloid inlays.",
                        8100)
        };
    }
}
