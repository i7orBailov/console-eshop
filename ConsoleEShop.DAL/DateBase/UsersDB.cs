﻿using ConsoleEShop.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.DAL.DateBase
{
    public static class UsersDB
    {
        public static IEnumerable<Account> usersAccounts = new Account[]
        {
            new Account("vasya1", "vasilyPetrov@mail.mail"),
            new Account("sj5Nso4j3sa2", "ptuxerman@mail.mail"),
            new Account("kntr453ii2", "morgen.shtern@alisher.com"),
            new Account("babaSKanatopa", "izolda1967@mail.mail"),
            new Account("superpass1", "igorigor1@mail.mail"),
            new Account("vjqrhenjqibah", "undefiniteUser1@mail.mail"),
            new Account("petro19petro88", "petro.chmuh@mail.mail")
        };

        public static IEnumerable<Account> administratorsAccounts = new Account[]
        {
            usersAccounts.FirstOrDefault(u => u.Password.Equals("kntr453ii2") &&
                                              u.Email.Equals("morgen.shtern@alisher.com")),
            usersAccounts.FirstOrDefault(u => u.Password.Equals("babaSKanatopa") &&
                                              u.Email.Equals("izolda1967@mail.mail"))
        };
    }
}
