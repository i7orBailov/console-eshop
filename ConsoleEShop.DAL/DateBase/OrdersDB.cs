﻿using ConsoleEShop.DAL.Entities;
using System.Collections.Generic;

namespace ConsoleEShop.DAL.DateBase
{
    public class OrdersDB
    {
        public IEnumerable<Order> OrdersAddedToWishList { get; set; } = new Order[] { };

        public IEnumerable<Order> OrdersToRegister { get; set; } = new Order[] { };
    }
}
