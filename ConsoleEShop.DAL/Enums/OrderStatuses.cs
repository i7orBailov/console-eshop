﻿
namespace ConsoleEShop.DAL.Enums
{
    public enum OrderStatuses
    {
        New,
        CancelledByAdministrator,
        CancelledByUser,
        PaymentReceived,
        Sent,
        Received,
        Completed
    }
}
