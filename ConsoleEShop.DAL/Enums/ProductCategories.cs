﻿namespace ConsoleEShop.DAL.Enums
{
    public enum ProductCategories
    {
        AcousticGuitars,
        ElectricGuitars,
        ElectricBasses,
        GuitarAmplifiers
    }
}
