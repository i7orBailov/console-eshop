﻿
using ConsoleEShop.DAL.Enums;

namespace ConsoleEShop.DAL.Entities
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ProductCategories Category { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }

        public Product(string name, ProductCategories category, string description, decimal price)
        {
            Name = name;
            Category = category;
            Description = description;
            Price = price;
        }
    }
}
