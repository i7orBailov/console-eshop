﻿
namespace ConsoleEShop.DAL.Entities
{
    public class Account
    {
        public string Password { get; set; }
        public string Email { get; set; }

        public Account(string email)
        {
            Email = email;
        }

        public Account(string password, string email) : this(email)
        {
            Password = password;
        }
    }
}
