﻿using System;
using ConsoleEShop.DAL.Enums;

namespace ConsoleEShop.DAL.Entities
{
    public class Order
    {
        public int Id { get; }
        public OrderStatuses OrderStatus { get; set; }
        public Product Product { get; set; }
        public Account Client { get; set; }
        public DateTime TimeOfOrdering { get; set; }
        
        public Order(OrderStatuses orderStatus, Product product, Account client, DateTime timeOfOrdering, int previousId)
        {
            OrderStatus = orderStatus;
            Product = product;
            Client = client;
            TimeOfOrdering = timeOfOrdering;
            Id = ++previousId;
        }
    }
}
