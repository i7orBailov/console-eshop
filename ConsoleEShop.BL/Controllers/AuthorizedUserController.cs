﻿using ConsoleEShop.BL.Services;
using ConsoleEShop.DAL.Enums;
using System;

namespace ConsoleEShop.BL.Controllers
{
    public class AuthorizedUserController
    {
        readonly AuthorizedUser service = new AuthorizedUser();
        
        public bool CreateNewOrder(string productName, string clientEmail)
        {
            HandleArgumentsException(productName, clientEmail);
            return service.CreateNewOrder(productName, clientEmail);
        }

        public bool ProcessOrder(int orderId, string clientEmail)
        {
            HandleArgumentsException(orderId, clientEmail);
            return service.ProcessOrder(orderId, clientEmail);
        }

        public bool SetOrderStatus(int orderId, string clientEmail)
        {
            HandleArgumentsException(orderId, clientEmail);
            return service.SetOrderStatus(orderId, clientEmail, OrderStatuses.Received);
        }

        public bool CancelOrder(int orderId, string clientMail)
        {
            HandleArgumentsException(orderId, clientMail);
            return service.CancelOrder(orderId, clientMail);
        }

        public bool CheckIfUserIsAdmin(string password, string mail)
        {
            HandleArgumentsException(password, mail);
            return service.CheckIfUserIsAdmin(password, mail);
        }

        void HandleArgumentsException(string productName, string clientEmail)
        {
            if (string.IsNullOrWhiteSpace(productName))
                throw new ArgumentNullException($"{nameof(productName)} was null or empty.");
            if (string.IsNullOrWhiteSpace(clientEmail))
                throw new ArgumentNullException($"{nameof(clientEmail)} was null or empty.");
        }

        void HandleArgumentsException(int orderId, string clientEmail)
        {
            if (orderId > 0)
                throw new ArgumentException($"{nameof(orderId)} was less than 0");
            if (clientEmail is null)
                if (string.IsNullOrWhiteSpace(clientEmail))
                    throw new ArgumentNullException($"{nameof(clientEmail)} was null or empty.");
        }
    }
}
