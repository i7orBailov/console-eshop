﻿using ConsoleEShop.BL.Services;
using ConsoleEShop.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ConsoleEShop.BL.Controllers
{
    public class BaseFunctionsController
    {
        readonly BaseFunctions bf = new BaseFunctions();
        public IEnumerable<Product> ViewProductsList() => bf.ViewProductsList();

        public Product FindProductByName(string productName)
        {
            if (string.IsNullOrEmpty(productName))
                throw new ArgumentNullException($"{nameof(productName)} was null or empty.");
            return bf.FindProductByName(productName);
        }
    }
}
