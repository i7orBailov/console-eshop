﻿using System;
using ConsoleEShop.BL.Services;

namespace ConsoleEShop.BL.Controllers
{
    public class VisitorController
    {
        readonly Visitor visitor = new Visitor();

        public bool Register(string password, string email)
        {
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException($"{password} was null or empty.");
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException($"{email} was null or empty.");
            return visitor.Register(password, email);
        }

        public bool LogIn(string password, string email)
        {
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException($"{password} was null or empty.");
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException($"{email} was null or empty.");
            return visitor.LogIn(password, email);
        }
    }
}
