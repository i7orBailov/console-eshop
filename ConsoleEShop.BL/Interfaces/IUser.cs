﻿using ConsoleEShop.DAL.Entities;
using System.Collections.Generic;

namespace ConsoleEShop.BL.Interfaces
{
    internal interface IUser
    {
        IEnumerable<Product> ViewProductsList();
        Product FindProductByName(string productName);
    }
}
