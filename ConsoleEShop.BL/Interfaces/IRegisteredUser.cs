﻿using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Enums;

namespace ConsoleEShop.BL.Interfaces
{
    interface IRegisteredUser : IUser
    {
        bool CreateNewOrder(string productName, string clientEmail);
        bool ProcessOrder(int orderId, string clientEmail);
        bool CancelOrder(int orderId, string clientEmail);
        bool SetOrderStatus(int orderId, string clientEmail, OrderStatuses orderStatus);
    }
}
