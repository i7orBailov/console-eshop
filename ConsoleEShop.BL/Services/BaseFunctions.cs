﻿using ConsoleEShop.BL.Interfaces;
using ConsoleEShop.DAL.DateBase;
using ConsoleEShop.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleEShop.BL.Services
{
    public class BaseFunctions : IUser
    {
        public IEnumerable<Product> ViewProductsList() =>
            ProductsDB.Products;

        public Product FindProductByName(string productName) =>
            ProductsDB.Products.FirstOrDefault(p => p.Name == productName);
    }
}
