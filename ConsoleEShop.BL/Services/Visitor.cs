﻿using ConsoleEShop.DAL.DateBase;
using System.Linq;
using ConsoleEShop.DAL.Entities;

namespace ConsoleEShop.BL.Services
{
    public class Visitor
    {
        public bool Register(string password, string email)
        {
            var currentUser = UsersDB.usersAccounts.FirstOrDefault(u => u.Password.Equals(password)
                                                                && u.Email.Equals(email));
            if (currentUser is null)
            {
                UsersDB.usersAccounts = UsersDB.usersAccounts.Append(new Account(password, email));
                return true;
            }
            else
                return false;
        }

        public bool LogIn(string password, string email)
        {
            var currentUser = UsersDB.usersAccounts.FirstOrDefault(u => u.Password.Equals(password)
                                                                && u.Email.Equals(email));
            return !(currentUser is null); 
        }
    }
}
