﻿using ConsoleEShop.BL.Interfaces;
using ConsoleEShop.DAL.DateBase;
using ConsoleEShop.DAL.Entities;
using ConsoleEShop.DAL.Enums;
using System.Linq;
using System;
using System.Collections.Generic;

namespace ConsoleEShop.BL.Services
{
    class AuthorizedUser : BaseFunctions, IRegisteredUser
    {
        readonly OrdersDB ordersCollection = new OrdersDB();

        public bool CancelOrder(int orderId, string clientEmail)
        {
            Order order = ordersCollection.OrdersAddedToWishList.FirstOrDefault(o => o.Id.Equals(orderId));
            if (order is null)
                return false;
            else
            {
                var wishList = ordersCollection.OrdersAddedToWishList as List<Order>;
                wishList.Remove(order);
                return true;
            }
        }

        public bool CreateNewOrder(string productName, string clientEmail)
        {
            Product product = ProductsDB.Products.FirstOrDefault(p => p.Name.Equals(productName));
            Account client = UsersDB.usersAccounts.FirstOrDefault(c => c.Email.Equals(clientEmail));
            if (product is null || client is null)
                return false;
            else
            {
                int orderLastId = ordersCollection.OrdersAddedToWishList.DefaultIfEmpty().Max(o => o?.Id) ?? 0;
                Order order = new Order(OrderStatuses.New,
                                        product,
                                        client,
                                        DateTime.Now,
                                        orderLastId);
                ordersCollection.OrdersAddedToWishList = ordersCollection.OrdersAddedToWishList.Append(order);
                return true;
            }
        }

        public bool ProcessOrder(int orderId, string clientEmail)
        {
            Order order = ordersCollection.OrdersAddedToWishList.FirstOrDefault(o => o.Id.Equals(orderId));
            if (order is null)
                return false;
            else
            {
                ordersCollection.OrdersToRegister = ordersCollection.OrdersToRegister.Append(order);
                return true;
            }
        }

        public bool SetOrderStatus(int orderId, string clientMail, OrderStatuses orderStatus)
        {
            Order order = ordersCollection.OrdersToRegister.FirstOrDefault(o => o.Id.Equals(orderId));
            if (order is null)
                return false;
            else
            {
                ordersCollection.OrdersToRegister =
                    ordersCollection.OrdersToRegister.Select(o => o == order ?
                                                             o = new Order(OrderStatuses.Received,
                                                                           o.Product,
                                                                           o.Client,
                                                                           o.TimeOfOrdering,
                                                                           o.Id - 1) : o);
                return true;
            }
        }

        public bool CheckIfUserIsAdmin(string password, string mail) =>
            UsersDB.administratorsAccounts.Contains(new Account(password, mail));
    }
}
